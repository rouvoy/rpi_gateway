Table of existing active local clusters with their location and their associated cluster number.

| **Existing Clusters** | **Corresponding Number**  | 
| ------------- |:-------------:| 
| Leuven (BE)      | 1 | 
| Neuchâtel (CH)      | 2      |
| Aveyron (FR) | 12      |

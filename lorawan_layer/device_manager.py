import sys
from flip_layer.logger import logger
import sqlite3 as lite
from lorawan_layer import lorawan_basic_util
from lorawan_layer import lorawan_crypto
from lorawan_layer import lorawan_message_handle
import codecs
import struct
import datetime
import config
import random
import collections
from lorawan_layer.lorawan_socket_handle import gw_id
import asyncio
from config import LoRaWAN_DEVICE_MANAGER_ED_MAINTENANCE_INTERVAL,END_DEVICE_CLEANING_PERIOD

## @package device_manager
#  Contains two main classes called Device Manager and End_Device to manage LoRaWAN operation and channel utilization
##


## Device_Manager..
#  Deals with all the device registration, handling and managing end devices in lorawan layer
#

class Device_Manager:
    ## Initialize device manager and configure database connection.
    def __init__(self):
        self.device_list=[]
        try:
            self.con = lite.connect(config.DB_NAME)
        except:
            logger.error("Device Database setup", "Couldn't connect device database")
        self.con.commit()
        self.cur = self.con.cursor()
        self.cur.execute('SELECT SQLITE_VERSION()')
        data_version = self.cur.fetchone()
        logger.log("Device Database setup","Connected to device database")
        self.get_dev_list_from_db()

    ##Gets the registired owned device from the database and create device objects and device list
    def get_dev_list_from_db(self):
        self.cur.execute("SELECT * FROM owned_eds")
        rows = self.cur.fetchall()
        for i in range(len(rows)):
            end_device = End_Device(rows[i][1],rows[i][2],rows[i][3])
            end_device.owned = True;
            end_device.handle = False;#for testing purposes only
            self.device_list.append(end_device)
        #debugggg
        # for i in range(len(self.device_list)):
        #     print (self.device_list[5].appkey)
        #     print (self.device_list[5].appeui)
        #     print (self.device_list[5].deveui)

    ##Check given deveui with the dev_list and returns device type and object, if a match is found
    #used in joining procedure and handling new device handler info
    #@param dev_eui
    #@returns a device type and object , if the device is on the device list
    def check_device_deveui(self,deveui):
        for i in range(len(self.device_list)):
            if self.device_list[i].deveui == deveui:
                if self.device_list[i].owned == True:
                    if self.device_list[i].handle == True:
                        return 'handle_owned',self.device_list[i]
                    else:
                        return 'owned',self.device_list[i]
                else:
                    if self.device_list[i].handle == True:
                        return 'handle',self.device_list[i]
                    else:
                        return 'unknown_device', self.device_list[i]
        return 'unknown_device',None

    ##Check given devaddr with the dev_list and returns device type and object, if a match is found
    #used in encrpytion/decryption of payload (owned and handled device)
    #@param dev_addr
    #@returns a device type and object , if the device is on the device list
    def check_device_devaddr(self,devaddr):
        for i in range(len(self.device_list)):
            if self.device_list[i].devaddr == devaddr:
                if self.device_list[i].owned == True:
                    if self.device_list[i].handle == True:
                        return 'handle_owned',self.device_list[i]
                    else:
                        return 'owned',self.device_list[i]#never hit here
                else:
                    if self.device_list[i].handle == True:
                        return 'handle',self.device_list[i]
                    else:
                        return 'unknown_device', self.device_list[i]
        return 'unknown_device',None

    ##Creates encrypted join acknowledgement message and set a channel for given end_device object(also sets a new device addr)
    #@param end_device device object
    #@param channel selected channel for uplink communication
    #returns encrypted join_ack message
    def create_join_ack(self,end_device,channel):
        ##test and debuggin purpose only
        self.cur.execute("SELECT max(Devaddr) as Devaddr FROM owned_eds")
        max_dev_addr = self.cur.fetchone()
        ##

        truncated_cluster_id = gw_id.cluster_id & 0b1111
        dev_addr_rand = random.getrandbits(15)
        dev_addr = int((truncated_cluster_id << 28) | (gw_id.gateway_id << 15) | dev_addr_rand)

        dev_type = 'known_device'
        while dev_type != 'unknown_device':
            dev_type, dev_obj = self.check_device_devaddr(dev_addr)

        new_devaddr = dev_addr

        join_ack,nwkskey,appskey=lorawan_message_handle.join_accept_new(end_device.dev_nonce,new_devaddr,channel,codecs.decode(end_device.appkey,'hex'))
        end_device.nwkskey = nwkskey
        end_device.appskey = appskey
        end_device.devaddr = format(new_devaddr,'x')
        logger.log("LoRaWAN Device Manager", "A Join Ack is created")

        return join_ack

    ##Verifies MIC of a join request message
    #@param end_device device object
    #@returns boolean(True/false) if MIC is valid
    def check_mic_joinreq(self,end_device,msg,msg_type):
        mhdr =  lorawan_message_handle.MACHeader(lorawan_message_handle.JOIN_REQUEST, lorawan_message_handle.LORAWAN_R1)


        data = mhdr.encode()  + struct.pack('<QQH', int(end_device.appeui,16),
                                                        int(end_device.deveui, 16) ,end_device.dev_nonce)
        aesdata = lorawan_crypto.aesEncrypt(codecs.decode(end_device.appkey,'hex'), (data), mode='CMAC')

        mic = format(struct.unpack('<L', aesdata[:4])[0],'x')


        return mic == end_device.mic

    ##Verifies MIC of an uplink message
    #@param end_device device object
    #@pre_msg header of the particular message that is needed to check to MIC
    #@returns boolean(True/false) if MIC is valid
    def check_mic_uplink_message(self,end_device,pre_msg):
        mhdr =  lorawan_message_handle.MACHeader(lorawan_message_handle.UN_DATA_UP, lorawan_message_handle.LORAWAN_R1)
        msg = mhdr.encode() + pre_msg
        B0 = struct.pack('<BLBLLBB',
                         int('0x49', 16), 0, 0, int(end_device.devaddr,16),
                         end_device.seq_nbr, 0, len(msg))
        data = B0 + msg

        aesdata = lorawan_crypto.aesEncrypt(lorawan_basic_util.intPackBytes(end_device.nwkskey,16), data, mode='CMAC')

        raw_mic = aesdata[:4]

        return raw_mic == end_device.mic

    ##Decrypt given encrypted LoRaWAN payload
    #@param end_device device object
    #@param msg message to be decrypted
    #@returns unencrypted string payload
    def decrypt_payload(self,end_device,msg):
        fpappmsg = lorawan_message_handle.decrypt_appmsg(end_device.appskey,end_device.devaddr,end_device.seq_nbr,msg)
        logger.log("LoRaWAN Device Manager", "An Uplink Message is Decrypted")

        #logger.log ("LoRaWAN Device Manager",fpappmsg)
        #lorawan_basic_util.msg2string((fpappmsg))
        return fpappmsg

    ##Saves unencrpyted payload to a file(creates a new file from deveui or writes into an existing file if it is already created
    #@param end_device device object
    #@param msg unencrypted payload
    #@returns True(if it is succesfull)
    def save_payload(self,end_device,msg):
        time = datetime.datetime.now()
        file_name = end_device.deveui + "_" + time.strftime("%Y-%m-%d")+ '.txt'
        file = open(file_name,'a')
        file.write(time.isoformat() + " " + msg.decode('utf-8')+ '\n' )
        file.close()
        logger.log("LoRaWAN Device Manager", "A Decrypted Uplink Message is saved into a file ")
        return True#or false

    ##Gets statistics and information about end_devices(only the ones that sent join req message and received by the gateway_
    #@returns device_list(info:deveui,rssi,channel,sf,handle/owned)
    def get_stats(self):
        list = {}
        # list['owned_end_device'] = []
        # list['handled_end_device'] = []
        list['heard_end_devices'] = []

        # #owned
        # for i in range(len(self.device_list)):
        #     if self.device_list[i].owned == True:
        #         list['owned_end_device'].append(self.device_list[i].deveui)
        # #handle
        # for i in range(len(self.device_list)):
        #     if self.device_list[i].handle == True:
        #         list['handled_end_device'].append(self.device_list[i].deveui)
        for i in range(len(self.device_list)):
            json_end_dev = {
                "deveui": 0,
                "rssi": 0,
                "channel": 0,
                "sf": 0,
                "handled": False,
                "owned": False,
                "dev_addr":0,

            }
            json_end_dev["deveui"]=self.device_list[i].deveui
            json_end_dev["rssi"]=self.device_list[i].rssi
            json_end_dev["channel"]=self.device_list[i].channel
            json_end_dev["sf"]=self.device_list[i].sf
            json_end_dev["handled"]=self.device_list[i].handle
            json_end_dev["owned"]=self.device_list[i].owned
            json_end_dev["dev_addr"]=self.device_list[i].devaddr
            list['heard_end_devices'].append(json_end_dev)
        #print(list['heard_end_devices'])


        return list

    ##Registers end_devices to the device_list
    #If the end_device is already registered, it finds the device object and update the nwkskey and ownership/handle status
    #If it is not registered, creates a new device object and appends to the device list
    #@param dev_eui
    #@param nwkskey (new networksession key)

    def register_unknown_dev_profile(self,devaddr):
        new_end_device = End_Device(None, 0, None)
        new_end_device.nwkskey = 0
        new_end_device.handle = False
        new_end_device.devaddr = devaddr
        self.device_list.append(new_end_device)

    def register_device(self,deveui,nwkskey,newdevaddr):
        dev_type,dev_obj=self.check_device_deveui(deveui);
        if (dev_type == 'unknown_device'):
            new_end_device = End_Device(None, deveui, None)
            new_end_device.nwkskey = nwkskey
            new_end_device.handle = False;
            self.device_list.append(new_end_device)
        else:
            dev_obj.nwkskey = nwkskey;
            dev_obj.handle = True;
        logger.log("LoRaWAN Device Manager", "Device Registration is Done")
    def check_dev_nonce(self,dev_obj,new_nonce):
        for i in range(len(dev_obj.old_dev_nonce)):
            if (dev_obj.old_dev_nonce[i] == new_nonce):
                return False
        return True
    def clean_old_device_data(self):
        for i in range(len(self.device_list)):
            if(self.device_list[i].owned == False):
                last_msg_time = self.device_list[i].last_msg_time
                current_time = datetime.datetime.now()
                diff = current_time - last_msg_time
                if diff.total_seconds() > END_DEVICE_CLEANING_PERIOD * 3600:
                    del self.device_list[i]
                    logger.log("LoRaWAN Device Manager", "An Old end-device is removed")
    async def maintenance_task(self):
        while True:
            logger.log("LoRaWAN Device Manager", "Running Maintanance Task ")
            self.clean_old_device_data()
            logger.log("LoRaWAN Device Manager", "Maintanance Task Done")
            await asyncio.sleep(LoRaWAN_DEVICE_MANAGER_ED_MAINTENANCE_INTERVAL)

## End_Device
#   End device object that is used by device manager to control all the information.
#   Upon registiration or handling a device object is created for each device to keep track of information.
#   @param appeui application eui
#   @param deveui device eui
#   @param appkey application key
class End_Device:

    def __init__(self,appeui,deveui,appkey):
        self.appeui = appeui
        self.appkey = appkey
        self.deveui = deveui
        self.rssi = None
        self.channel = None
        self.handle = None
        self.owned = None
        self.appskey = None
        self.nwkskey = None
        self.msg = None
        self.fcounter = None
        self.devaddr = None
        self.seq_nbr = None
        self.dev_nonce = None
        self.old_dev_nonce = collections.deque(maxlen=32)
        self.mic = None
        self.sf = None
        self.last_msg_time = None




if __name__ == "__main__":
    manager = Device_Manager()
    manager.get_dev_list_from_db()

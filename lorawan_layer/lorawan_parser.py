#!/usr/bin/env python3
import socket
import json
import base64
import struct
import time
import codecs
import sys
from lorawan_layer.config_lorawan_layer import UDP_IP,UDP_PORT,UDP_PORT_DOWN,ACK_JSON,RSSI_OFFSET
from flip_layer.logger import logger,MsgColour
import threading

cur_version = sys.version_info  # base64.decode differs from python 2.7 to 3.x

device_info = {
    "rssi": 0,
    "snr":0,
    "channel": 0,
    "sf": 0,
}
PULL_RESP_ID = 0
new_ts = 0
new_dr = 0
new_ch = 0
lorawan_missed_join_ack_count = 0
missed_join_ack = True
start_time = 0
timeout = 0
reset_timer = 0
## @package lorawan_parser
# Communicates with the reference implementation(LoRa transciver drivers), parse received packets, sends downlink messages
#@param cur_version to check current python version to just print values for debugging purpose
##


## LoRaWAN packet parser..
#
#
# Communicates with reference implementation via UDP sockets, parse received packets, send join accepts messages
# @param send_packet A Queue for sync communication with upper layer for packet to be sent(now only join accept) to end device
# @param received_packet A Queue for sync communication with upper layer for packet to be sent to end device


#  Lora Packet GW-Server Bytes
#   0 version = 1
#   1-2random token
#   3 PUSH_DATA identifier 0x00
#   4-11 Gateway unique identifier (MAC address)
#   12-end JSON object, starting with {, ending with }, see section 4
#   Size (Bytes):     1      |   1 ~ M     |  4
#   PHY Payload	: MAC Header | MAC Payload | MIC
#   Bit#	:     7..5     |    4..2   | 1..0
#   MAC Header	: Message Type |    RFU    | Major
#   Size (Bytes) :    7..23     |   0..1     |    0..N
#   MAC Playload : Frame Header | Port Field | Frame Payload
#   * Port Field: 0 - Network Session Key; 1 - Application Session Key
#   Size (Bytes) :       4        |       1       |       2       |     0..15
#   Frame Header : Device Address | Frame Control | Frame Counter | Frame Options
#   Bit#          :          7         |     6     |  5  |     4    |   3..0
#   Frame Control : Adaptive Data Rate | ADRACKReq | ACK | FPending | FOptsLen
##

def lorawan_parser(send_packet, received_packet):
    global PULL_RESP_ID
    global new_ts
    global new_dr
    global new_ch
    global missed_join_ack
    global start_time
    global reset_timer
    #stats variables
    lorawan_received_packet_total_count = 0
    lorawan_uplink_msg_count = 0
    lorawan_join_msg_count = 0
    packet_forwarder_stats_packet_total_count = 0
    packet_forwarder_stats_forwarded_total_count = 0
    global lorawan_missed_join_ack_count
    number_of_messages_to_dispatcher = 0
    sock = socket.socket(socket.AF_INET, # Internet
                         socket.SOCK_DGRAM) # UDP
    sock.bind((UDP_IP, UDP_PORT))

    try:
        lorawan_join_ack_thread = threading.Thread(name='lorawan parser join ack',
                                                   target=lorawan_join_ack,
                                                   args=(send_packet,))
        lorawan_join_ack_thread.daemon = True;
        lorawan_join_ack_thread.start()
        logger.log("LoRaWAN Parser", "Running LoRaWAN Join Ack Thread ",MsgColour.INIT)
        missed_join_ack = False
    except:
        logger.error("LoRaWAN Parser", "LoRaWAN layer Threading Problem in parser(join ack thread)",MsgColour.INIT)
        sys.exit()




    logger.log("lorawan parser","Waiting LoRa messages ...",MsgColour.INIT)
    while True:
        data, addr = sock.recvfrom(4096) # buffer size is 1024 bytes
        if len(data) > 12:
            bdata = struct.unpack("!BBBBBBBBBBBB", memoryview(data[0:12]))
            # print ("Protocol Version : %X" % bdata[0])
            # print ("Random Token     : %X" % ((bdata[1] << 8) | bdata[2]))
            # print ("PUSH Data ID     : %X" % bdata[3])
            # GatewayID = (bdata[4] << 56) | (bdata[5] << 48) | (bdata[6] << 40) | (bdata[7] << 32) | (bdata[8] << 24) | (bdata[9] << 16) | (bdata[10] << 8) | bdata[11]
            # print ("Gateway ID       : %X" % GatewayID)
            if(bdata[3] == 0):
                json_data = data[12:]

            else:
                json_data = data[4:]

            # if(bdata[1] == 0):
            #     json_data = data[4:]
            #
            # else:
            #     json_data = data[12:]
            try:
                ojson = json.loads(json_data)
            except:
                logger.error ('Lorawan Parser','there is problem with the json_data')
            if 'rxpk' in ojson:
                logger.log("Lorawan Parser", "Message received (lowest layer)",MsgColour.BOLD)

                for p in ojson["rxpk"]:
                    lorawan_received_packet_total_count+=1
                    msg = base64.b64decode(p["data"],altchars=None)
                    rssi = p["rssi"]
                    snr = p["lsnr"]
                    channel = p["chan"]
                    sf =  p["datr"]
                    message = ""
                    device_info['rssi'] = rssi - RSSI_OFFSET
                    device_info['snr'] = snr
                    device_info['channel'] = channel
                    device_info['sf'] = sf

                    if (cur_version >= (3, 0)):
                        for char in msg:  # FOR EACH BYTE
                            message += '{:02X}'.format(char)
                    else:
                        for char in msg:  # FOR EACH CHAR
                            message += '{:02X}'.format(ord(char))
                    if message.startswith("40") and len(message) > 8:
                        lorawan_uplink_msg_count+=1
                        #logger.log("LoRaWAN parser", "put info tp the queue")

                        received_packet.put(msg,block=False)
                        received_packet.join()
                        received_packet.put(device_info,block=False)
                        received_packet.join()
                        #logger.log("LoRaWAN parser", "queue job is done")




                    elif message.startswith("00") and len(message) > 8:
                        lorawan_join_msg_count+=1
                        #logger.log("LoRaWAN parser(Join ack)", "put info the queue")

                        received_packet.put(msg,block=False)
                        received_packet.join()
                        received_packet.put(device_info,block=False)
                        received_packet.join()
                        start_time = time.time()
                        try:
                            reset_timer = 0
                            lorawan_join_ack_thread = threading.Thread(name='lorawan parser join ack timer',
                                                                       target=join_ack_timer)
                            lorawan_join_ack_thread.daemon = True;
                            missed_join_ack = False
                            lorawan_join_ack_thread.start()
                            logger.warn("LoRaWAN Parser", "DEBUG: Running LoRaWAN Join Ack Timer AND missed_join_ack is FALSE")
                        except Exception as e:
                            logger.error("LoRaWAN Parser", "LoRaWAN layer Threading Problem in parser(join ack thread)")
                            logger.error("LoRaWAN Parser", str(e))
                            sys.exit()
                        #logger.log("LoRaWAN parser", "queue job is done")


                        bdata = struct.unpack("!BBBBBBBBBBBB", memoryview(data[0:12]))

                        if bdata[3] == 0:
                            PULL_RESP_ID = "02405003"

                            ts = ojson['rxpk'][0]['tmst']
                            new_ts = ts + 5000000
                            new_dr = ojson['rxpk'][0]['datr']
                            new_ch = ojson['rxpk'][0]['freq']


            elif 'stat' in ojson:
                packet_forwarder_stats_packet_total_count+=ojson['stat']['rxok']
                packet_forwarder_stats_forwarded_total_count+=ojson['stat']['rxfw']
                logger.log ("Packet Forwarder stats","Stats from gateway:",MsgColour.INIT)
                logger.log ("Packet Forwarder stats","Number of radio packets received with a valid PHY CRC in last 30sec : " + str(ojson['stat']['rxok']),MsgColour.INIT)
                logger.log ("Packet Forwarder stats","Number of radio packets forwarded in last 30sec : " + str(ojson['stat']['rxfw']),MsgColour.INIT)
                logger.log ("Packet Forwarder stats","Number of radio packets received in last 30sec : " + str(ojson['stat']['rxnb']),MsgColour.INIT)
                logger.log ("Packet Forwarder stats","Number of packets emitted in last 30sec : " + str(ojson['stat']['txnb']),MsgColour.INIT)

                logger.log ("Packet Forwarder stats","Number of packets received with a valid CRC : " + str(packet_forwarder_stats_packet_total_count),MsgColour.INIT)
                logger.log ("Packet Forwarder stats","Number of radio packets forwarded : " + str(packet_forwarder_stats_forwarded_total_count),MsgColour.INIT)

                logger.log ("Radio layer(LoRaWAN Parser) stats","Number of Join messages : " + str(lorawan_join_msg_count),MsgColour.INIT)
                logger.log ("Radio layer(LoRaWAN Parser) stats","Number of Uplink messages : " + str(lorawan_uplink_msg_count),MsgColour.INIT)
                logger.log ("Radio layer(LoRaWAN Parser) stats","Total Number of messages : " + str(lorawan_received_packet_total_count),MsgColour.INIT)
                logger.log ("Radio layer(LoRaWAN Parser) stats","Number of Missed/Rejected Join Acks : " + str(lorawan_missed_join_ack_count),MsgColour.INIT)


                #print ('------------------------')

def lorawan_join_ack(send_packet):
    data2 = bytearray(1024)
    sock2 = socket.socket(socket.AF_INET,
                          socket.SOCK_DGRAM)
    sock2.bind((UDP_IP, UDP_PORT_DOWN))
    global PULL_RESP_ID
    global new_ts
    global new_dr
    global new_ch
    global lorawan_missed_join_ack_count
    global missed_join_ack
    global timeout
    global reset_timer
    # logger.log("LoRaWAN Parser", "DEBUG: ENTERING JOIN ACK LOOP")
    while True:
        if not send_packet.empty():
            logger.warn("LoRaWAN Parser", "DEBUG: JOIN ACK LOOP QUEUE NOT EMPTY")
            if not missed_join_ack:
                nbytes, addr = sock2.recvfrom_into(data2, 1024)
                logger.warn("LoRaWAN Parser", "DEBUG: missed_join_ack is FALSE")
                if (nbytes > 0):
                    # start_time = time.time()
                    logger.warn("LoRaWAN Parser", "DEBUG: ENTERING SECOND LOOP")
                    timeout = 0
                    while send_packet.empty():
                        # measure_time = time.time()
                        # time_elapsed = measure_time - start_time
                        # if time_elapsed > 6:
                        #     timeout = 1
                        #     break
                        time.sleep(1)
                    logger.warn("LoRaWAN Parser", "DEBUG: EXITING SECOND LOOP")
                    # if timeout:
                    #     lorawan_missed_join_ack_count += 1
                    #     missed_join_ack = True
                    #     logger.log("Lorawan parser", "No Join Ack sent (lowest layer)", MsgColour.INIT)
                    #     break
                    #logger.log("LoRaWAN Dispatcher", "get info from queue")
                    msg = send_packet.get()
                    send_packet.task_done()
                    #logger.log("LoRaWAN Dispatcher", "queue job is done")

                    json_packet = json.loads(ACK_JSON)
                    json_packet['txpk']['data'] = base64.b64encode(msg).decode('utf-8')
                    json_packet['txpk']['tmst'] = new_ts
                    json_packet['txpk']['datr'] = new_dr
                    json_packet['txpk']['freq'] = new_ch

                    bytes1 = json.dumps(json_packet)
                    final_message = codecs.decode(PULL_RESP_ID, 'hex').decode("utf-8") + bytes1
                    reset_timer = 1
                    sock2.sendto(final_message.encode('utf-8'), addr)

                    logger.log("Lorawan Parser", "Join accept message sent (lowest layer)", MsgColour.BOLD)
            else:
                #logger.log("LoRaWAN Parser", "cleaning missed join acks")
                logger.warn("LoRaWAN Parser", "DEBUG: MISSED join ACK")
                missed_join_ack = False
                msg = send_packet.get()
                send_packet.task_done()
                del msg
                #logger.log("LoRaWAN Parser", "queue job is done")

            #break

    #logger.log("Lorawan Parser", "Join Ack Thread Done")


def join_ack_timer():
    global timeout
    global lorawan_missed_join_ack_count
    global missed_join_ack
    global reset_timer
    start_time = time.time()
    while True:
        if not reset_timer:
            current_time = time.time()
            time_elapsed = current_time - start_time
            if time_elapsed > 6:
                timeout = 1
                lorawan_missed_join_ack_count += 1
                missed_join_ack = True
                logger.log("Lorawan parser", "No Join Ack sent (lowest layer)", MsgColour.INIT)
                break
            time.sleep(1)
        else:
            break
    return

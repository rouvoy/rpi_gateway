# FLIP

FLIP is a distributed federation connecting LoRaWAN gateways.  
Each gateway joins a cluster corresponding to a given geographic area, and will collaborate and share resources with other participating gateways.  
Clusters are interconnected allowing global roaming capabilities without having to previously register the roaming devices. 

## Getting Started

These instructions cover the preparation of a RaspberryPi-based LoRaWAN gateway as well as setting up FLIP.

### Hardware Prerequisites

Our gateways are built on this setup:

- Raspberry Pi 3 Model B
- iC880A-SPI - LoRaWAN Concentrator 868 MHz can be found [here](https://shop.imst.de/wireless-modules/lora-products/8/ic880a-spi-lorawan-concentrator-868-mhz)
- Corresponding pigtail cable and antenna
- Power supply, female-to-female jumper wires, SD card 8GB+ with Raspbian

Please follow [this tutorial](https://www.instructables.com/id/Raspberry-Pi-LoRaWAN-Gateway/) on how to connect the concentrator board with the RPi.

### Software Prerequisites

- Install Raspbian Full on the SD card and don't forget to change the default password !
- Enable SPI via `raspi-config` as show [here](https://www.raspberrypi-spy.co.uk/2014/08/enabling-the-spi-interface-on-the-raspberry-pi/)
- Install preliminary tools:  
`sudo apt-get install sqlite3 liblzo2-dev`  

- Install [tinc](https://www.tinc-vpn.org/download/) (tested with v1.0.36):  
`wget https://www.tinc-vpn.org/packages/tinc-1.0.36.tar.gz && tar xvf tinc-1.0.36.tar.gz && cd tinc-1.0.36 && ./configure && make && sudo make install`  

- Create the system unit files for tinc:    
In `/lib/systemd/system/tinc.service`

> [Unit]  
> Description=Tinc VPN  
> After=network.target  
>   
> [Service]  
> Type=oneshot  
> RemainAfterExit=yes  
> ExecStart=/bin/true  
> ExecReload=/bin/true  
> WorkingDirectory=/etc/tinc  
>   
> [Install]  
> WantedBy=multi-user.target  

In `/lib/systemd/system/tinc@.service`

> [Unit]  
> Description=Tinc net %i  
> PartOf=tinc.service  
> ReloadPropagatedFrom=tinc.service  
>   
> [Service]  
> Type=simple  
> WorkingDirectory=/etc/tinc/%i  
> ExecStart=/usr/local/sbin/tincd -c /etc/tinc/%i -n %i -D  
> ExecReload=/usr/local/sbin/tincd -c /etc/tinc/%i -n %i -kHUP  
> KillMode=mixed  
> TimeoutStopSec=5  
> Restart=always  
> RestartSec=60  
>   
> [Install]  
> WantedBy=tinc.service  

then `sudo systemctl unmask tinc && sudo mkdir -p /usr/local/var/run`

- Retreive last version of FLIP:  
`cd ~/ && git clone https://gitlab.com/FLIP_federation/rpi_gateway.git FLIP && cd FLIP && sudo pip3 install -r requirements.txt`  

- Install packet_forwarder and lora_gateway from Semtech:  
`git clone https://github.com/Lora-net/lora_gateway.git && cd lora_gateway && make && cd ..`  
`git clone https://github.com/Lora-net/packet_forwarder.git && cd packet_forwarder && make && cd ..`

## Setup instructions

- Initialise FLIP with 
`./run_flip_gw.sh --init --cluster <YOUR_CLUSTER_NUMBER> --name <YOUR_GW_NAME>`
where <YOUR_CLUSTER_NUMBER> being one of the corresponding existing cluster detailed in [clusters.md](clusters.md)
and <YOUR_GW_NAME> a unique identifier different from any existing one in [hosts.md](hosts.md)
- Populate the keys corresponding to your end-devices in the file [eds.own](eds.own) 

## Run FLIP

For a normal run, simply run FLIP with `./run_flip_gw.sh`.   
For a more verbose mode, add the `-v` flag, the corresponding logs can be found under `/tmp/FLIP/`. Please note that network activity logging will require you to install the `tcpdump` additional package.

from flip_layer.gateway_stub import GatewayStub
import os
import ipaddress

# Retreive our assigned cluster number and our cluster ID from sourced config file
cluster_num = int(os.environ["GW_CLUSTER"])
gateway_num = int(os.environ["GW_ID"])

# Common config
HOST = "10.1." + str(cluster_num) + "." + str(gateway_num)
CLUSTER_NAME = "CLUSTER" + str(cluster_num)
CLUSTER_ID =  cluster_num      # 8-bit identifier for cluster.
GATEWAY_ID =  cluster_num*100+gateway_num   # 13-bit identifier of gateway within cluster.
IS_LEADER = (gateway_num == 2)


bootstrapping_gateways = [
    GatewayStub(host = "10.1." + str(cluster_num) + "." + str(i), flip_high_server_port ="9002", \
                flip_high_server_leader_port="9003", cluster_name = CLUSTER_NAME)
        for i in range(1,254) if i != gateway_num if os.system("ping -c 1 10.1."+ str(cluster_num) + "." + str(i) + " > /dev/null 2>&1") == 0
]

# Number of max possible clusters limited at 20 for now. Cluster 0 reserved for analysis/maintenance ops.
CLUSTER_DEFINITIONS = {
    "CLUSTER"+str(i): ipaddress.IPv4Network("10.1."+str(i)+".0/24")
        for i in range(1,20)
}


# Variable to generate different configs for testing purposes

try:
    DB_NAME = os.environ['ED_DB']
except Exception:
    pass

LEADER_ONLY = False

FLIP_STATS_LOG_INTERVAL = 10*60
LoRaWAN_STATS_LOG_INTERVAL = 10*60

#LoRaWANLayer Settings
LoRaWAN_DEVICE_MANAGER_ED_MAINTENANCE_INTERVAL = 3600 #seconds
END_DEVICE_CLEANING_PERIOD = 10 #hours

MAX_RSSI = -46					# Floor value used to calculate a given RSSI from a distance on the grid
MIN_RSSI = -140                 # Accurate?
MAX_RADIO_RANGE = 80			# Radio range max expressed in distance on the grid
NUM_CHANNELS = 8                # Number of channels supported by gateway

# Common kNN settings
K = 3                               # k for kNN
KNN_REFRESH_RATE = 60 			    # Delay in seconds between two rounds of KNN/RPS
KNN_CYCLE = 3600				    # Delay in seconds to relaunch a KNN classification cycle after having converged
KNN_CONVERGENCE_THRESHOLD = 5       # Number of kNN rounds the kNN set should be stable to be considered as converged
AGING_INTERVAL = 1                  # Delay in seconds between subsequent increments of stub age counter used to detect freshness

LORAWAN_POLL_CYCLE = 5*60           # Polling interval (seconds) for communication with LoRaWAN layer

SUB_DURATION = 12*60*60             # Time (seconds) for which a gateway subscribes to an end device
SUB_DELAY = 8*60*60                 # Delay (seconds) between subsequent transmissions of subscriptions for an owned end device
MAX_SUB_MESSAGE_AGE = 300           # Minimum time (seconds) before a gateway forgets about a message that is being disseminated.
ED_MAINTENANCE_INTERVAL = 180       # Time interval (seconds) between two checks by behaviour.EDMaintenance to remove stale messages/subscriptions

CONSENSUS_MESSAGE_EXPIRY = 10       # Delay (seconds) after which consensus protocol for a given end device is considered to be over.

NUM_HELD_SUBS = 4                   # Number of subs to maintain at owner to be used for recency check

CLUSTER_INTERCONNECT_INTERVAL = 300 # Delay between subsequent IP range scans to find leaders of remote clusters

## The port at which the LoRaWAN layer provides service
LORAWAN_SERVER_PORT = 9000

## The port at which the FLIP layer provides service to the LoRaWAN layer
FLIP_LOW_SERVER_PORT =  9001

## The port at which the FLIP layer provides service to the FLIP layer at remote gateways
FLIP_HIGH_SERVER_PORT = 9002

## The port at which the FLIP layer provides leader service to the FLIP layer at remote gateways
FLIP_HIGH_SERVER_LEADER_PORT = 9003




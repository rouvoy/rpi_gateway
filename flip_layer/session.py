import json
import yaml
import socket
import asyncio
import re
from datetime import datetime, timedelta

import flip_layer.gateway_stub as gateway_stub
from flip_layer.logger import logger, MsgColour

import config

# Endpoint for stateful asynchronous conversation over a TCP connection.
class Session:

    ## Initiate a session.
    # @param loop Asyncio loop to run on
    # @connection TCP socket for the connection
    def __init__(self, loop, connection):
        self.loop = loop
        self.connection = connection
        self.hasEnded = False

    # Asyncio coroutine that reads a message from self.connection.
    # Data is expected to be encoded as a single utf-8 encoded json object terminated by a newline.
    # Binary data is expected to be base64 encoded. This method expects only one message to be sent at a time.
    # @return A dictionary obtained by parsing the first message read from the socket.
    async def listen(self):
        if self.connection is None:
            logger.error("FLIP-LoRaWAN session", "Connection not initiated")
            return
        frags = []
        while True:
            frag = await self.loop.sock_recv(self.connection, 4096)
            frags.append(frag)
            if (len(frag) == 0) or (frag[-1] == ord('\n')):
                break
        raw_message = (b''.join(frags).decode("utf-8")).strip()
        logger.log("FLIP-LoRaWAN session", "Received raw message: " + raw_message)

        try:
            parsed_msg = yaml.load(raw_message)
            assert isinstance(parsed_msg, dict)
        except Exception as err:
            logger.error("FLIP-LoRaWAN session", "Failed to parse message " + raw_message)
            self.connection.close()
            raise err

        return parsed_msg

    ## Send a message to the other end of the connection.
    # @param data Dictionary that contains the data to send
    # The message format used is described for self.listen().
    # As self.listen() relies on termination of a message by a newline, binary data should be base64 encoded and
    # self.talk() should be called at most once before the recipient has called self.listen().
    def talk(self, data):
        if self.connection is None:
            logger.error("FLIP-LoRaWAN session", "Connection not initiated")
            return
        try:
            assert isinstance(data, dict)
        except AssertionError:
            logger.error("FLIP-LoRaWAN session", "Invalid answer format")
            return
        self.connection.sendall(bytes(json.dumps(data, ensure_ascii = False) + "\n", "utf-8"))

    ## End the conversation by closing the connection.
    def end(self):
        try:
            self.connection.close()
        except Exception:
            pass
        self.hasEnded = True

## Handle request from LoRaWAN layer.
class DispatcherSession(Session):

    def __init__(self, loop, connection, gw):
        super().__init__(loop, connection)
        self.gw = gw

    ## Asyncio routine that handles an incoming service request from the LoRaWAN layer, possibly returns data over
    # self.connection and ends the session when done.
    async def dispatch_message(self):

        try:
            parsed_msg = await self.listen()
        except Exception as  e:
            logger.error("Dispatch", "No good answer: " + str(e))
            return

        try:
            assert isinstance(parsed_msg["event"], str)
        except Exception as err:
            logger.error("Dispatch", "Message does not contain event field")
            self.connection.close()
            return

        logger.log("Dispatch", "Received message for event: " + parsed_msg["event"], colour_prepend = MsgColour.LORAWAN_EVENT)

        # Possible handler receives join request from LoRaWAN layer
        if parsed_msg["event"] == "join_request":
            self.gw.stats.counters["join_requests_received_from_lorawan"] += 1
            ed_id = parsed_msg["ed_id"]
            lorawan_data = parsed_msg["lorawan_data"]
            #Ensure consistency with cached ed_data
            self.gw.ed_data["heard_end_devices"][ed_id] = {
                "rssi": parsed_msg["rssi"],
                "channel": parsed_msg["channel"],
                "sf": sf_conversion(parsed_msg["sf"])
            }

            logger.log("Dispatch", "Notified of join request for end device " + ed_id)
            if ed_id in self.gw.ed_maintenance.subs:
                o = str(gateway_stub.GatewayStub.get_identifier_from_info(self.gw.ed_maintenance.subs[ed_id]["owner_info"]))
                logger.log("Dispatch", "Sub found for end device " + ed_id + " owned by " + o)
                occ = self.gw.calculate_occupation_on_handle(ed_id)
                self.gw.ed_maintenance.add_join_attempt(ed_id, occ, lorawan_data)
                message = self.gw.ed_maintenance.create_local_consensus_message_for_join(ed_id, occ)
                logger.log("Dispatch", "About to participate in consensus: prospective occupation " + str(message["occupation"]) + ", channels " + \
                            str(list(message["channel_occupation"].values())[0]))
                await self.gw.local_consensus(message)
            else:
                logger.log("Dispatch", "No sub found for end device, dropping " + ed_id)

        # Receive device overview from LoRaWAN layer
        elif parsed_msg["event"] == "pass_stats":
            devices = parsed_msg["heard_end_devices"]
            logger.log("Dispatch", "Received ed data: " + str(devices))
            converted = get_flip_representation_of_ed_data(devices)
            logger.log("Dispatch", "FLIP representation of ed_data: " + str(converted))
            self.gw.ed_data = converted

        # LoRaWAN layer provides networking data (keys etc) to be passed to new device handler
        elif parsed_msg["event"] == "new_handler_response":
            stub = self.gw.ed_maintenance.ed_handlers[parsed_msg["ed_id"]]
            handler_accept_message = self.gw.ed_maintenance.create_handler_accept_message(
                parsed_msg["ed_id"],
                parsed_msg["lorawan_data"],
                str(datetime.now() + timedelta(hours=1)))
            await stub.handler_accept(handler_accept_message)

        # LoRaWAN layer provides application data to be passed to device owner
        elif parsed_msg["event"] == "device_data_for_owner":
            # TODO use rssi etc to update Gateway.ed_data?
            self.gw.stats.counters["data_messages_received_from_lorawan"] += 1
            ed_id = parsed_msg["ed_id"]
            try:
                stub = gateway_stub.GatewayStub.create_from_info(self.gw, self.gw.ed_maintenance.subs[ed_id]["owner_info"])
                message =  self.gw.ed_maintenance.create_device_data_message(ed_id, parsed_msg["lorawan_data"])
                self.gw.stats.counters["data_messages_sent_to_owner"] += 1
                await stub.device_data(message)
            except Exception as e:
                logger.error("Dispatch", "Error looking up owner: " + str(e))

        else:
            self.talk({"error": "Unknown event!"})
            logger.error("Dispatcher", "Unknown event: " + parsed_msg["event"])

        self.end()

## Communicate with LoRaWAN layer to request some service.
# Do not use constructor directly, obtain a session through LoRaWANClientSession.obtain()
class LoRaWANClientSession(Session):
    def __init__(self, loop, gw):
        super().__init__(loop, None)
        self.gw = gw

    ## Asyncio coroutine that returns a usable session when connection with the lower layer has successfully
    # been established.
    # @return A LoRaWANClientSession providing service from  the LoRaWAN layer
    @staticmethod
    async def obtain(loop, gw):
        s = LoRaWANClientSession(loop, gw)
        await s.initiate()
        return s

    async def initiate(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setblocking(True)

        while True:
            try:
                sock.connect((config.HOST, config.LORAWAN_SERVER_PORT))
                break
            except ConnectionRefusedError as err:
                logger.error("LoRaWAN client", "Cannot reach LoRaWAN layer")
                await asyncio.sleep(3, self.loop)

        self.connection = sock

    ## Ask LoRaWAN layer to pass a device overview, as specified in lorawan_spoof.py at a later point in time
    def get_stats(self):
        data =  {
                "event": "get_stats",
                "cluster_id": self.gw.cluster_id,
                "gateway_id": self.gw.gateway_id
                }
        self.talk(data)
        logger.log("LoRaWAN Client", "sending get_stats event ", colour_prepend = MsgColour.LORAWAN_EVENT)
        self.end()

    ## Inform LoRaWAN layer of new handler, asking it to invoke a new_handler_response event at some later point in time
    # @param ed_id Globally unique identifier for device that should be handled
    # @param lorawan_data Data passed to FLIP by LoRaWAN layer of handler on join request
    # @param channel Channel that should be suggested in join ack (number)
    def new_handler(self, ed_id, lorawan_data, channel):
        data = {
            "event": "new_handler",
            "ed_id": ed_id, #
            "lorawan_data": lorawan_data,
            "channel": channel
        }
        self.talk(data)
        logger.log("LoRaWAN Client", "sending new_handler event (requesting session keys and corresponding join ack) ", colour_prepend = MsgColour.LORAWAN_EVENT)
        self.end()

    ## Inform LoRaWAN layer at handler that it should handle a device and provide it with corresponding metadata (join ack, keys etc)
    # @param ed_id Globally unique identifier for device that should be handled
    # @param lorawan_data Data passed to FLIP by LoRaWAN layer of owner in corresponding new_handler_response event
    # @param expires String (str(datetime.datetime.now()) detailing when handler can stop handling
    def install_handler(self, ed_id, lorawan_data):
        self.gw.stats.counters["handler_installs"] += 1
        data = {
            "event": "install_handler",
            "ed_id": ed_id,
            "lorawan_data": lorawan_data,
        }
        self.talk(data)
        logger.log("LoRaWAN Client", "sending install_handler event (installing session keys and passing join ack)", colour_prepend = MsgColour.LORAWAN_EVENT)
        self.end()

    ## Inform LoRaWAN layer that the handler of an owned device received new application data
    # @param ed_id Globally unique identifier for that end device
    # @param lorawan_data Data passed to FLIP by LoRaWAN layer at handler within an device_data_for_owner event
    def device_data_from_handler(self, ed_id, lorawan_data):
        data = {
            "event": "device_data_from_handler",
            'ed_id': ed_id,
            "lorawan_data": lorawan_data
        }
        self.talk(data)
        logger.log("LoRaWAN Client", "sending device_data_from_handler event ", colour_prepend = MsgColour.LORAWAN_EVENT)
        self.end()

## Convert device overview provided by LoRaWAN layer to format that is used by FLIP layer
# @param ed_data overview as provided by LoRaWAN layer (as in lorawan_spoof.py)
# @returns overview as used by FLIP layer (as commented out in lorawan_spoof.py)
def get_flip_representation_of_ed_data(ed_data):
    devices = ed_data
    result = {}
    result["owned_end_devices"] = [d["deveui"] for d in devices if d["owned"] and d["deveui"]]
    result["handled_end_devices"] = [d["deveui"] for d in devices if d["handled"] and d["deveui"]]
    result["heard_end_devices"] = {
        d["deveui"]: {
            "rssi": d["rssi"],
            "sf": sf_conversion(d["sf"]),
            "channel": d["channel"]
        }
        for d in devices
            if ((d["rssi"] is not None) and (d["sf"] is not None) and (d["channel"] is not None) and (d["deveui"] is not None))
    }
    result["heard_end_devices_dev_addr"] = {
        d["dev_addr"]: {
            "rssi": d["rssi"],
            "sf": sf_conversion(d["sf"]),
            "channel": d["channel"]
        }
        for d in devices
            if ((d["rssi"] is not None) and (d["sf"] is not None) and (d["channel"] is not None) and d["dev_addr"])
    }
    return result

def sf_conversion(lorawan_representation_str):
    sf_matcher = re.compile("SF(\d+)B")
    return int(sf_matcher.match(lorawan_representation_str).group(1))

